#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 23 22:03:08 2018

@author: vijay
"""

import analytics_assignment as aa


original_data_path ="/home/vijay/Downloads/test/Sample Dataset.csv"

fuzzy_cleaned_data_path ="/home/vijay/Downloads/test/cleanedfuzzy_0.90.csv"
simple_cleaned_data_path ="/home/vijay/Downloads/test/cleanedsimple_0.90.csv"

aa.fuzzy_string_matcher(original_data_path, fuzzy_cleaned_data_path,threshold = 0.90)

aa.simple_string_matcher(original_data_path, simple_cleaned_data_path,threshold = 0.90)
