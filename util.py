#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 23 21:47:20 2018

@author: vijay
"""
import difflib
import re
import jellyfish
import numpy as np

def process_str_for_similarity_cmp(input_str, normalized=False, ignore_list=[]):
    for ignore_str in ignore_list:
        input_str = re.sub(r'{0}'.format(ignore_str), "", input_str, flags=re.IGNORECASE)

    if norma#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 23 21:47:20 2018

@author: vijay
"""
import difflib
import re
import jellyfish
import numpy as np

def process_str_for_similarity_cmp(input_str, normalized=False, ignore_list=[]):
    '''Ignore all the special characters and also ignore the case '''
    for ignore_str in ignore_list:
        input_str = re.sub(r'{0}'.format(ignore_str), "", input_str, flags=re.IGNORECASE)
    '''cleans special chars and extra whitespace '''    
    if normalized is True:
        input_str = input_str.strip().lower()
        input_str = re.sub("\W", "", input_str).strip()

    return input_str



def find_string_similarity(first_str, second_str, normalized=False, ignore_list=[]):
    first_str = process_str_for_similarity_cmp(first_str, normalized=normalized, ignore_list=ignore_list)
    second_str = process_str_for_similarity_cmp(second_str, normalized=normalized, ignore_list=ignore_list)
    match_ratio = (difflib.SequenceMatcher(None, first_str, second_str).ratio() + jellyfish.jaro_winkler((first_str), (second_str)))/2.0
    return match_ratiolized is True:
        input_str = input_str.strip().lower()
        #clean special chars and extra whitespace
        input_str = re.sub("\W", "", input_str).strip()

    return input_str



def find_string_similarity(first_str, second_str, normalized=False, ignore_list=[]):
    first_str = process_str_for_similarity_cmp(first_str, normalized=normalized, ignore_list=ignore_list)
    second_str = process_str_for_similarity_cmp(second_str, normalized=normalized, ignore_list=ignore_list)
    match_ratio = (difflib.SequenceMatcher(None, first_str, second_str).ratio() + jellyfish.jaro_winkler((first_str), (second_str)))/2.0
    return match_ratio