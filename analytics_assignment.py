#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb 23 21:48:41 2018

@author: vijay
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 20 20:40:10 2018

@author: vijay
"""

import pandas as pd
import difflib
import re
import jellyfish
import numpy as np
from fuzzywuzzy import fuzz

import util as ut

def simple_string_matcher(original_data_directory,cleaned_data_directory,threshold = 0.90):
    drop_indexi=[]
    data = pd.read_csv(original_data_directory)
    #Converts all the dates to the same format
    data['dob'] = pd.to_datetime(data.dob)
    ##Drop the nan values
    data = data.dropna()
    #drop the rows with incomplete rows
    #////////    
    #Delete all the rows that are exact duplicates of each other, retaining 
    #just the first one
    data = data.drop_duplicates()    
    s = data.index.tolist()
    
    for t,i in enumerate(s):
        for j in s[(t+1):]:
            #find the similarity only if the gender and dob match
            if(data.loc[i]['gender'] == data.loc[j]['gender'] \
               and data.loc[i]['dob'] == data.loc[j]['dob']):
                if(ut.find_string_similarity(data.loc[i]['first_name']+data.loc[i]['last_name'],\
                                          data.loc[j]['first_name']+data.loc[j]['last_name'],\
                                          normalized= True)>threshold):
                    drop_indexi.append(i)
    data = data.drop(drop_indexi)
    data.to_csv(cleaned_data_directory)
    
'''Below function uses a python library called fuzzywuzzy to do fuzzy string
    matching. We find the average of the ratio, quick ratio,token_sort_ratio
and token_set_ratio and apply a threshold to avoid duplicates '''
def fuzzy_string_matcher(original_data_directory,cleaned_data_directory,threshold = 0.90):
    drop_indexi  = []
    data = pd.read_csv(original_data_directory)
    #Converts all the dates to the same format
    data['dob'] = pd.to_datetime(data.dob)
    ##Drop the nan values
    data = data.dropna()
    #drop the rows with incomplete rows
    #////////    
    #Delete all the rows that are exact duplicates of each other, retaining 
    #just the first one
    data = data.drop_duplicates()    
    s = data.index.tolist()
    
    for t,i in enumerate(s):
        for j in s[(t+1):]:
            
            if(data.loc[i]['gender'] == data.loc[j]['gender'] \
               and data.loc[i]['dob'] == data.loc[j]['dob']):
                      
                
               ratio= (fuzz.ratio(data.loc[i]['first_name']+data.loc[i]['last_name'],\
                                  data.loc[j]['first_name']+data.loc[j]['last_name']))/100
               pratio= (fuzz.partial_ratio(data.loc[i]['first_name']+" "+data.loc[i]['last_name'],\
                                  data.loc[j]['first_name']+" "+data.loc[j]['last_name']))/100
               tokensortratio= (fuzz.token_sort_ratio(data.loc[i]['first_name']+" "+data.loc[i]['last_name'],\
                                  data.loc[j]['first_name']+" "+data.loc[j]['last_name']))/100
               tokensetratio= (fuzz.token_set_ratio(data.loc[i]['first_name']+" "+data.loc[i]['last_name'],\
                                  data.loc[j]['first_name']+" "+data.loc[j]['last_name']))/100
                                                            
               avg =(ratio+pratio+tokensortratio+tokensetratio)/4
               if(avg>threshold):
                 drop_indexi.append(i)  
               #counter += 1
                       
    data = data.drop(drop_indexi)
    data.to_csv(cleaned_data_directory)